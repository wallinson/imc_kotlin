package com.ufrn.imd.primeira_app_kotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import kotlinx.android.synthetic.main.activity_resultado.*


class resultado : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_resultado)
        var appBar = supportActionBar
        appBar!!.title = "Resultado IMC"

        Log.i("LOG",intent.getStringExtra("classe"))
        Log.i("LOG","${intent.getIntExtra("imagem",R.drawable.fat)}")

        classeImc.setText(intent.getStringExtra("classe"))
        imgResultado.setImageResource(intent.getIntExtra("imagem",R.drawable.fat))
    }



}
