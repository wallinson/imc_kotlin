package com.ufrn.imd.primeira_app_kotlin
import android.app.ActionBar
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*


class Main : AppCompatActivity() {

    var classeImc = "";
    var imagem = 0;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        var appBar = supportActionBar
        appBar!!.title = "IMC em Kotlin"
    }


    fun calcular(view: View) {
        if( pesoEditText.text.isNotEmpty() && alturaEditText.text.isNotEmpty()){
            //val peso = pesoEditText.text.toString().toDouble()
            var peso = java.lang.Double.parseDouble("${pesoEditText.text}")
            val altura = alturaEditText.text.toString().toDouble()

            imc(peso, altura)

            clearAll()

            var intent = Intent(this, resultado::class.java)
            intent.putExtra("imagem",imagem)
            intent.putExtra("classe",classeImc)
            startActivity(intent)

        }else{
            Toast.makeText(this, "Preencha os campos em branco!!!", Toast.LENGTH_LONG).show()
        }

    }

    fun clearAll(){
        pesoEditText.setText("")
        alturaEditText.setText("")
    }

    fun imc (peso : Double, altura: Double) {
        val imc = peso / (altura * altura)
        val imcStr = String.format("%.2f",imc)

        if (imc < 18.6) {
            classeImc = "Abaixo do peso (${imcStr})"
            imagem = R.drawable.thin
        } else if (imc >= 18.6 && imc < 24.9) {
            classeImc = "Peso ideal (${imcStr})"
            imagem = R.drawable.musc
        } else if (imc >= 24.9 && imc < 29.9) {
            classeImc = "Levemente acima do peso (${imcStr})"
            imagem = R.drawable.fat
        } else if (imc >= 29.9 && imc < 34.9) {
            classeImc = "Obesidade Grau I (${imcStr})"
            imagem = R.drawable.fat
        }else if (imc >= 34.9 && imc < 39.9) {
            classeImc = "Obesidade Grau II (${imcStr})"
            imagem = R.drawable.fat
        } else if (imc >= 40){
            classeImc = "Obesidade Grau III (${imcStr})"
            imagem = R.drawable.fat
        }
    }

}